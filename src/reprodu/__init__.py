from getpass import getpass
from pathlib import Path

import pandas as pd

from bibliodbs.query_downloader import WOSQueryDownloader
from bibliodbs.biblio_wos import load_extracted_cleaned_and_enriched
from sashimi import GraphModels


def run(country):
    # Download data to current directory
    get_wqd(country).download()
    # Fit a domain-topic model
    corpus = get_sashimi(country)
    # Output a domain-topic block map
    corpus.domain_map()


#############
# get_query #
#############


def get_query(country, local_terms=None):
    # Always include English terms
    terms_en = [
        "reproducible",
        "reproducibility",
        "irreproducible",
        "irreproducibility",
    ]

    # Replace localterms if it's not a list
    if local_terms == None:
        local_terms = []
    if local_terms == "France":
        local_terms = [
            "reproductible",
            "reproductibilité",
            "irreproductible",
            "irreproductibilité",
        ]
    if local_terms == "Brazil":
        local_terms = [
            "reprodutível",
            "reprodutibilidade",
            "irreprodutível",
            "irreprodutibilidade",
        ]

    # Build and return the query
    query_country = f'"{country}"'
    query_terms = " OR ".join(f'"{term}"' for term in (terms_en + local_terms))
    query = f"""
        (TI=({query_terms})
         OR AB=({query_terms})
         OR AK=({query_terms}))
        AND CU=({query_country})
        """
    return query


def get_outpath(country):
    return f"{country.casefold()}.jsonl.zst"


def get_wqd(country, local_terms=None, **kwargs):
    """
    Get the WOS query downloader.
    To download the data:
    ```
    wqd = get_wqd("Brazil")
    wqd.download()
    ```
    """
    wos_api_key = getpass("Please, enter your WOS API key: ")
    query = get_query(country, local_terms)
    outpath = get_outpath(country)
    return WOSQueryDownloader(wos_api_key, query=query, outpath=outpath, **kwargs)


###############
# get_sashimi #
###############


def load_data_as_frame(country, include_raw_records=False):
    records = load_extracted_cleaned_and_enriched(
        get_outpath(country), include_raw_records=include_raw_records
    )
    data = [*records]
    df = pd.DataFrame(data)
    df.attrs["name"] = country
    return df


def enhance_title(df):
    df["enhanced_title"] = df.agg(
        lambda x: x["title"] + "<br>[" + "; ".join(x["authors_full_name"]) + "]", axis=1
    )


def get_sashimi(country):
    """
    Data must be first downloaded with `get_wqd(country).download()`
    """
    df = load_data_as_frame(country)
    df = df.dropna(subset=["abstract", "title"])
    enhance_title(df)

    # Build a corpus for modeling with sashimi
    config_path = Path("auto_sashimi", "reports", f'{df.attrs["name"]}.json')
    if not config_path.exists():
        corpus = GraphModels(
            config_path=config_path,
            col_time="year",
            col_url=["url_doi", "url_wos"],
            col_title="enhanced_title",
            col_venue="venue",
            col_id="uid",
            text_sources=["title", "abstract"],
        )
    else:
        corpus = GraphModels(config_path=config_path, load_data=False)
    corpus.load_data(df, name=df.attrs["name"])
    corpus.process_sources(ngrams=3, language="en", stop_words=True)

    # Load a domain-topic model based on the textual tokens produced
    corpus.load_domain_topic_model()

    # Save the configuration for next time
    corpus.register_config()
    return corpus
