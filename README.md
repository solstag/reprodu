# Reprodu

A little project to map reprodu… in different countries using data from Web of Science and (soon) OpenAlex.

This project depends on [bibliodbs](https://gitlab.com/cortext/cortext-libraries/bibliodbs/) for downloading data and on [sashimi](https://gitlab.com/solstag/sashimi) for analyses.

## Running

``` bash
python3 -m pip install git+https://gitlab.com/solstag/reprodu.git
COUNTRY=France python3 -c "import reprodu; reprodu.run(\"${COUNTRY}\")"
```
